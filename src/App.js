import React, { Suspense, lazy, useState, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import { persistCache } from 'apollo-cache-persist'

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.js";
import TopMenu from "./components/TopMenu";
import Header from "./components/Header";
import Footer from "./components/Footer";
import "./App.min.css";

const HomeView = lazy(() => import("./views/Home"));
const ProductListView = lazy(() => import("./views/product/List"));
const ProductDetailView = lazy(() => import("./views/product/Detail"));
const NotFoundView = lazy(() => import("./views/pages/404"));
const InternalServerErrorView = lazy(() => import("./views/pages/500"));

const cache = new InMemoryCache({})

const errorLink = onError(({ graphqlErrors, networkError }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({ message, location, path }) => {
      return alert(`Graphql error ${message}`);
    });
  }
  return
});

const link = from([
  errorLink,
  new HttpLink({ uri: "http://localhost:4242/graphql" }),
]);

const client = new ApolloClient({
  cache: cache,
  link: link,
});

async function setupPersistence() {
  try {
    await persistCache({
      cache: cache,
      storage: window.localStorage
    })
  } catch (err) {
    console.log(err)
  }
}

function App() {

  const [hydrated, setHydrated] = useState(false)

  useEffect(() => {
    setupPersistence()
      .finally(() => setHydrated(true))
  }, [])

  if (!hydrated)
    return <p>loading our persisted cache...</p>

  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <React.Fragment>
          <Header />
          <TopMenu />
          <Suspense
            fallback={
              <div className="text-white text-center mt-3">Loading...</div>
            }
          >
            <Switch>
              <Route exact path="/" component={HomeView} />
              <Route exact path="/category/:category(Fashion|Supermarket|Electronics|Jewellery)" component={ProductListView} />
              <Route exact path="/product/detail/:productid(\d+)" component={ProductDetailView} />
              <Route exact path="/500" component={InternalServerErrorView} />
              <Route component={NotFoundView} />
            </Switch>
          </Suspense>
          <Footer />
        </React.Fragment>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
