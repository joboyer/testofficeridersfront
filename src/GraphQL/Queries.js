import { gql } from "@apollo/client";

export const LOAD_PRODUCT = gql`
query ($id: Int!) {
    getProductById(id: $id){
        productCategory,
        productId,
        productName,
        productImage,
        productStock,
        productPrice,
        ProductTva,
        productRating,
        productEan,
        productDescription
    }
}
`;

export const LOAD_ALSO_LIKE = gql`
query ($category: String!, $id: Int!) {
    getAlsoLike(productCategory: $category, id: $id){
        productId,
        productCategory,
        productName,
        productImage,
        productStock,
        productPrice,
        ProductTva,
        productRating,
        productEan,
        productDescription
    }
}
`;

export const LOAD_PRODUCT_BY_CAT = gql`
query ($category: String!) {
    getProductByCat(productCategory: $category){
        productId,
        productCategory,
        productName,
        productImage,
        productStock,
        productPrice,
        ProductTva,
        productRating,
        productEan,
        productDescription
    }
}
`;