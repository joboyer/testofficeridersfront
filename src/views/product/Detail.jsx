import React, { lazy, useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { LOAD_PRODUCT } from "../../GraphQL/Queries";
import { ReactComponent as IconStarFill } from "bootstrap-icons/icons/star-fill.svg";
import { useParams } from "react-router";
import { Redirect } from "react-router-dom";
const CardFeaturedProduct = lazy(() =>
  import("../../components/card/CardFeaturedProduct")
);

function ProductDetailView() {
  let { productid } = useParams();
  const [id] = useState(parseInt(productid))
  const [product, setProduct] = useState([]);

  // LOAD PORUDCT FROM GRAPHQL API
  const { error, loading, data } = useQuery(LOAD_PRODUCT, {
    variables: { id },
  });

  useEffect(() => {
    if (data) {
      setProduct(data.getProductById[0]);
    }
  }, [data, loading, error]);

  // Check if product ID exist
  const regexpProducId = new RegExp('^[1][0-4][0-9][0-9]$|^1500$');
  const regexpProducId1000 = new RegExp('^1000$');
  if (!regexpProducId.test(productid) || regexpProducId1000.test(productid)) {
    return <Redirect to='/500' />
  }


  return (
    <div className="container-fluid mt-3">
      <div className="row">
        <div className="col-md-8">
          <div className="row mb-3">
            <div className="col-md-5 text-center">
              <img
                src={product.productImage}
                className="img-fluid mb-3"
                alt=""
              />
            </div>
            <div className="col-md-7">
              <h1 className="h5 d-inline mr-2">
                {product.productName}
              </h1>
              <span className="badge bg-success mr-2"> New</span>
              <span className="badge bg-danger mr-2">Hot</span>
              <div className="mb-3">
                {Array.from({ length: product.productRating }, (_, key) => (
                  <IconStarFill className="text-warning mr-1" key={key} />
                ))}
              </div>
              <dl className="row small mb-3">
                <dt className="col-sm-3">Category</dt>
                <dd className="col-sm-9">{product.productCategory}</dd>
                <dt className="col-sm-3">Availability</dt>
                <dd className="col-sm-9">{product.productStock ? 'In stock' : 'Out of stock'}</dd>
                <dt className="col-sm-3">EAN</dt>
                <dd className="col-sm-9">{product.productEan}</dd>
              </dl>

              <div className="mb-3">
                <span className="font-weight-bold h5 mr-2">$ {product.productPrice} TTC</span>
                <span className="small text-muted mr-2"> TVA ${product.ProductTva} </span>
              </div>
              <div dangerouslySetInnerHTML={{
                __html: product.productDescription
              }}>

              </div>
            </div>
          </div>
          <div className="row">
          </div>
        </div>
        <div className="col-md-4">
          <CardFeaturedProduct data={product} />
        </div>
      </div>
    </div>
  );
}

export default ProductDetailView;
