import React, { lazy, useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { LOAD_PRODUCT_BY_CAT } from "../../GraphQL/Queries";
import { useParams } from "react-router";
const FilterCategory = lazy(() => import("../../components/filter/Category"));
const CardProductList = lazy(() =>
  import("../../components/card/CardProductList")
);

function ProductListView() {
  let { category } = useParams();
  const [products, setProducts] = useState([]);
  const { error, loading, data } = useQuery(LOAD_PRODUCT_BY_CAT, {
    variables: { category },
  });
  useEffect(() => {
    if (data) {
      setProducts(data.getProductByCat);
    }
  }, [data, loading, error]);

  return (
    <React.Fragment>
      <div
        className="p-5 bg-primary bs-cover"
        style={{
          backgroundImage: "url(../../images/banner/50-Banner.webp)",
        }}
      >
        <div className="container text-center">
          <span className="display-5 px-3 bg-white rounded shadow">
            {category}
          </span>
        </div>
      </div>
      <br />
      <div className="container-fluid mb-3">
        <div className="row">
          <div className="col-md-3">
            <FilterCategory />
          </div>
          <div className="col-md-9">
            <div className="row">
              <div className="col-md-8">
                <span className="align-middle font-weight-bold">
                  {products.length} results for{" "}
                  <span className="text-warning">"{category}"</span>
                </span>
              </div>
              <div className="col-md-4">

              </div>
            </div>
            <hr />
            <div className="row g-3">
              {
                products.map((product, idx) => {
                  return (
                    <div key={idx} className="col-md-12">
                      <CardProductList data={product} />
                    </div>
                  );
                })}
            </div>
            <hr />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default ProductListView;
