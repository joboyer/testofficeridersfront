import React from "react";
import { Link } from "react-router-dom";

const FilterCategory = (props) => {
  return (
    <div className="card mb-3">
      <div className="card-header font-weight-bold text-uppercase">
        Categories
      </div>
      <ul className="list-group list-group-flush">
        <li className="list-group-item">
          <Link to="/category/Fashion" className="text-decoration-none stretched-link">
            Fashion
          </Link>
        </li>
        <li className="list-group-item">
          <Link to="/category/Electronics" className="text-decoration-none stretched-link">
            Electronics
          </Link>
        </li>
        <li className="list-group-item">
          <Link to="/category/Jewellery" className="text-decoration-none stretched-link">
            Jewellery
          </Link>
        </li>
        <li className="list-group-item">
          <Link to="/category/Supermarket" className="text-decoration-none stretched-link">
            Supermarket
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default FilterCategory;
