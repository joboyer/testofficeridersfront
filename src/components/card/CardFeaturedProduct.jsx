import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { ReactComponent as IconStarFill } from "bootstrap-icons/icons/star-fill.svg";
import { LOAD_ALSO_LIKE } from "../../GraphQL/Queries";

const CardFeaturedProduct = (props) => {
  const category = props.data.productCategory;
  const id = props.data.productId;
  const [products, setProducts] = useState([]);
  const { error, loading, data } = useQuery(LOAD_ALSO_LIKE, {
    variables: { category, id },
  });

  useEffect(() => {
    if (data) {
      setProducts(data.getAlsoLike);
    }
  }, [data, loading, error]);
  return (
    <div className="card mb-3">
      <div className="card-header font-weight-bold text-uppercase">
        Featured Products
      </div>
      <div className="card-body">
        {products.map((product, idx) => (
          <div
            className={`row ${idx + 1 === products.length ? "" : "mb-3"}`}
            key={idx}
          >
            <div className="col-md-4">
              <img src={product.productImage} className="img-fluid" alt="..." />
            </div>
            <div className="col-md-8">
              <h6 className="text-capitalize mb-1">
                <a href={'/product/detail/' + product.productId} className="text-decoration-none">
                  {product.productName}
                </a>
              </h6>
              <div className="mb-2">
                {Array.from({ length: product.productRating }, (_, key) => (
                  <IconStarFill className="text-warning mr-1" key={key} />
                ))}
              </div>
              <span className="font-weight-bold h5">${product.productPrice}</span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CardFeaturedProduct;
